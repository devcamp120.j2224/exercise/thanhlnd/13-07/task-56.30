package com.devcamp.task5630.service;

import java.util.ArrayList;
import java.util.List;

import com.devcamp.task5630.model.CRegion;

public class CRegionService {
    private static List<CRegion> regionVietNam = new ArrayList<CRegion>();
    private static List<CRegion> regionUs = new ArrayList<CRegion>();
    private static List<CRegion> regionRussia = new ArrayList<CRegion>();

    static {
        CRegion dongnai = new CRegion("Đồng Nai", 60);
        CRegion tphochiminh = new CRegion("Sài Gòn", 59);
        CRegion hanoi = new CRegion("Hà Nội", 29);
        CRegion texas = new CRegion("Texax", 430);
        CRegion florida = new CRegion("Florida", 305);
        CRegion newyork = new CRegion("New York", 718);
        CRegion moscow = new CRegion("Moscow", 495);
        CRegion kaluga = new CRegion("Kaluga", 4842);
        CRegion saintperter = new CRegion("Saint Petersburg", 812);

        regionVietNam.add(dongnai);
        regionVietNam.add(tphochiminh);
        regionVietNam.add(hanoi);

        regionUs.add(texas);
        regionUs.add(florida);
        regionUs.add(newyork);

        regionRussia.add(moscow);
        regionRussia.add(kaluga);
        regionRussia.add(saintperter);

        
    }

    public static List<CRegion> getRegionVietNam() {
        return regionVietNam;
    }

    public static void setRegionVietNam(List<CRegion> regionVietNam) {
        CRegionService.regionVietNam = regionVietNam;
    }

    public static List<CRegion> getRegionUs() {
        return regionUs;
    }

    public static void setRegionUs(List<CRegion> regionUs) {
        CRegionService.regionUs = regionUs;
    }

    public static List<CRegion> getRegionRussia() {
        return regionRussia;
    }

    public static void setRegionRussia(List<CRegion> regionRussia) {
        CRegionService.regionRussia = regionRussia;
    }
}
